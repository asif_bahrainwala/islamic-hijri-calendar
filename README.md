* A simple birthday reminder as per Gregorian and Hijri calendar

### How do I get set up? ###

* Get QT5.12+
* open the .pro file in QTCreator
* build and run
* to send birthda y/ waras greetings via email, you must have Thunderbird (latest) installed and configured to use your email address.
* This application is tested only on Linux Mint 18, I am confident that it will be an easy port to Windows & Mac
