#include "dialog.h"
#include "ui_dialog.h"

//for strings
#define STR_MAX 1024

// --
// String class is used by string functions
// --

class String
{
public:
    char string[STR_MAX];

    String(const char *buffer="")
    {
        strcpy(string,buffer);
    }
    operator char*()
    {
        return string;
    }
    String operator =(char *str)
    {
        strcpy(string,str);
        return *this;
    }
};

typedef std::vector<String> stringArray;

using namespace std;
void Parse(vector<string> &ret,const string &s,const char *token_)
{
    string temp=s;
    const char *s1=strstr(s.c_str(),token_);
    if(!s1)
    {
        ret.push_back(s);return;
    }
    temp.at(s1-s.c_str())=0;
    ret.push_back(temp.c_str());
    Parse(ret,s1+strlen(token_),token_);
}

vector<string> tokenizer(const char *buffer,const char *tokenizer) //will fill 'ret' upon returning
{
    vector<string> r;
    Parse(r,buffer,tokenizer);
    return r;
}


/////////////////////////////////////
/// \brief Dialog::Dialog
/// \param parent
///

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    load();
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(TimerUpdate()));
    m_timer.start(1000);
}

Dialog::~Dialog()
{
    save();
    delete ui;
}

void Dialog::TimerUpdate()
{
    IslamicDateStruct qret=PrintCalender(QDate::currentDate());
    this->ui->calendarWidget_3->setSelectedDate(qret);

    String s;
    sprintf(s.string,"HijriDate:-  D:%d-M:%d-Y:%d",qret.d_,qret.m_,qret.y_);
    this->ui->calendarWidget_3->setToolTip(s.string);
}

void AddItemToList(QListView *qLv,QString str)
{
    QStandardItemModel *model = (QStandardItemModel*)qLv->model();
    if(model==0)
    {
        model=new QStandardItemModel(1,1);
        model->removeRow(0);
    }
    QStandardItem *item = new QStandardItem(str);model->appendRow(item);
    qLv->setModel(model);
}

void Dialog::on_pushButton_released()
{
    AddItemToList(ui->listView,ui->lineEdit->text());
    AddItemToList(ui->listView_2,ui->lineEdit_2->text());
    m_qdateList.push_back(ui->calendarWidget->selectedDate());
}

void updateTextBoxes(Dialog *diag,QModelIndex index)
{
    QStandardItemModel *model = (QStandardItemModel*)diag->ui->listView->model(); if(model==nullptr) return;
    QStandardItemModel *model_2 = (QStandardItemModel*)diag->ui->listView_2->model(); if(model_2==nullptr) return;

    diag->ui->lineEdit->setText(model->item(index.row())->text());
    diag->ui->lineEdit_2->setText(model_2->item(index.row())->text());

    diag->ui->calendarWidget->setSelectedDate(diag->m_qdateList[index.row()]);
    IslamicDateStruct isl = PrintCalender(diag->m_qdateList[index.row()]);
    diag->ui->calendarWidget_2->setSelectedDate(isl);

    String s;
    sprintf(s.string,"HijriDate:-  D:%d-M:%d-Y:%d",isl.d_,isl.m_,isl.y_);
    diag->ui->calendarWidget_2->setToolTip(s.string);
}

void Dialog::on_listView_2_clicked(const QModelIndex &index)
{
    ui->listView->selectionModel()->clearSelection();
    ui->listView->selectionModel()->select(index,QItemSelectionModel::Select);
    updateTextBoxes(this,index);
}

void Dialog::on_listView_clicked(const QModelIndex &index)
{
    ui->listView_2->selectionModel()->clearSelection();
    ui->listView_2->selectionModel()->select( index, QItemSelectionModel::Select );
    updateTextBoxes(this,index);
}

void Dialog::on_pushButton_2_released()
{
    QStandardItemModel *model = (QStandardItemModel*)ui->listView->model(); if(model==nullptr) return;
    QStandardItemModel *model_2 =(QStandardItemModel*) ui->listView_2->model(); if(model_2==nullptr) return;

    QModelIndexList list =ui->listView->selectionModel()->selectedIndexes();
    if(list.size()==0) return;

    model->removeRow(list[0].row());
    model_2->removeRow(list[0].row());
    m_qdateList.erase(m_qdateList.begin()+list[0].row());
}

void Dialog::on_pushButton_3_clicked()
{
    QMessageBox q(QMessageBox::Icon::NoIcon,"About","By Asif Bahrainwala,\n asif_bahrainwala@hotmail.com | 0091-9823018914\nHijri calendar birthday reminder");
    q.exec();
}
#define FILENAME "data.txt"
//save /load data from file
void Dialog::save()
{
    QStandardItemModel *model = (QStandardItemModel*)ui->listView->model(); if(model==nullptr) return;
    QStandardItemModel *model_2 =(QStandardItemModel*) ui->listView_2->model(); if(model_2==nullptr) return;

    FILE *fp=fopen(FILENAME,"w");
    if(fp)
    {
        for(unsigned int i=0;i<model->rowCount();++i)
            fprintf(fp,"%s:%s:%d,%d,%d\n",model->item(i)->text().toStdString().c_str(),model_2->item(i)->text().toStdString().c_str(),
                    this->m_qdateList[i].day(),this->m_qdateList[i].month(),this->m_qdateList[i].year());

        fclose(fp);
    }else
    {
        QMessageBox q(QMessageBox::Icon::NoIcon,"Error!","Cannot save data to file");
        q.exec();
    }
}

void Dialog::load()
{
    QStandardItemModel *model = (QStandardItemModel*)ui->listView->model();
    if(model==0)
    {
        model=new QStandardItemModel(1,1);
        model->removeRow(0);
    }

    QStandardItemModel *model_2 =(QStandardItemModel*) ui->listView_2->model();
    if(model_2==0)
    {
        model_2=new QStandardItemModel(1,1);
        model_2->removeRow(0);
    }

    FILE *fp=fopen(FILENAME,"r");
    if(fp)
    {
        String name;
        while(!feof(fp)){
            char * y1=fgets(name,sizeof(name),fp);
            if(y1==0) continue;

            printf("Data:%s\n",name.string);fflush(stdout);

            vector<string> r=tokenizer(name,":");
            QStandardItem *item = new QStandardItem(r[0].c_str());
            QStandardItem *item_2 = new QStandardItem(r[1].c_str());

            model->appendRow(item);model_2->appendRow(item_2);

            vector<string> date=tokenizer(r[2].c_str(),",");
            int d=atoi(date[0].c_str()),m=atoi(date[1].c_str()),y=atoi(date[2].c_str());

            const QDate q(y,m,d);
            this->m_qdateList.push_back(q);
        }
        fclose(fp);
    }else
    {
        printf("Error!!\n");fflush(stdout);
        QMessageBox q(QMessageBox::Icon::NoIcon,"Error!","Cannot load data from file (may be your first run)");
        q.exec();
    }
    ui->listView->setModel(model);
    ui->listView_2->setModel(model_2);
}

bool g_bSendBirthdayGrretings=true; //is this is set to true, it will send birthay greetings else waras mubaraks
//on_pushButton_5_clicked() uses it to tell on_pushButton_4_released to send birthday mails or wars mubarak
//hence on_pushButton_4_released,SendMailBirthday may will read this value (but will not modify),
//on_pushButton_5_clicked will set this value to false to send waras mubaraks mails and reset it back to true, when done, it will set it back to true

#include<mutex>
void* SendMailBirthday(const char *name,const char *email)
{
    static std::mutex mu;
    std::lock_guard<std::mutex> sync(mu);

    String buff;
    if(g_bSendBirthdayGrretings==false)
        sprintf(buff,"thunderbird -compose \"to='%s',subject='Waras Mubarak!!',body='Hi %s, Waras Mubarak, how are you doing?'\""
                ,email,name);
    else
        sprintf(buff,"thunderbird -compose \"to='%s',subject='Happy Birthday!!',body='Hi %s, Happy Birthday, how are you doing?'\""
                ,email,name);

    printf("%s\n",buff.string);fflush(stdout);
    system(buff.string);

    return nullptr;
}

#include<future>
void Dialog::on_pushButton_4_released()
{
    QStandardItemModel *model =(QStandardItemModel*) ui->listView->model();
    QStandardItemModel *model_2 = (QStandardItemModel*)ui->listView_2->model();

    if(model==nullptr||model_2==nullptr)return;

    IslamicDateStruct curr(QDate::currentDate());
    if(g_bSendBirthdayGrretings==false)
    {
        curr=::PrintCalender((QDate::currentDate()));
    }

    std::vector<std::future<void*>> arrFuture;

    for(int i=0;i<model->rowCount();++i){
        IslamicDateStruct birthday=this->m_qdateList[i];

        if(g_bSendBirthdayGrretings==false)
            birthday=::PrintCalender(this->m_qdateList[i]);

        if(birthday.m_==curr.m_ && birthday.d_==curr.d_)
        {
            String n=model->item(i)->text().toStdString().c_str();
            String e=model_2->item(i)->text().toStdString().c_str();
            arrFuture.push_back(std::async(SendMailBirthday,n,e));
        }
    }

    for(auto &x:arrFuture)
        x.get();
}

void Dialog::on_pushButton_5_released()
{
    printf("on_pushButton_5_released\n");fflush(stdout);
    g_bSendBirthdayGrretings=false;
    on_pushButton_4_released();
    g_bSendBirthdayGrretings=true;
}
//convert greogorian to hijri
//http://adnanumer.blogspot.in/2013/06/gregorian-to-hijri-calender-c-progarm.html
int LastDayOfGregorianMonth(int month, int year) {
    // Compute the last date of the month for the Gregorian calendar.

    switch (month) {
    case 2:
        if ((((year % 4) == 0) && ((year % 100) != 0))
                || ((year % 400) == 0))
            return 29;
        else
            return 28;
    case 4:
    case 6:
    case 9:
    case 11: return 30;
    default: return 31;
    }
}

int calcAbsGregorianDays(int d, int m, int y) {
    int N = d;
    for (int i = m - 1; i > 0; i--)
        N += LastDayOfGregorianMonth(i, y);

    return N + (y - 1) * 365
            + (y - 1) / 4
            - (y - 1) / 100
            + (y - 1) / 400;
}

bool IsIslamicLeapYear(int year) {
    // True if year is an Islamic leap year

    if ((((11 * year) + 14) % 30) < 11)
        return true;
    else
        return false;
}

int LastDayOfIslamicMonth(int month, int year) {
    // Last day in month during year on the Islamic calendar.

    if (((month % 2) == 1) || ((month == 12) && IsIslamicLeapYear(year)))
        return 30;
    else
        return 29;
}

const int IslamicEpoch = 227014; // Absolute date of start of Islamic calendar

int IslamicDate(int month, int day, int year) {
    return (day                      // days so far this month
            + 29 * (month - 1)       // days so far...
            + month/2                //            ...this year
            + 354 * (year - 1)       // non-leap days in prior years
            + (3 + (11 * year)) / 30 // leap days in prior years
            + IslamicEpoch);                // days before start of calendar
}
IslamicDateStruct PrintCalender(const QDate &qd){
    int d,m,y;
    qd.getDate(&y,&m,&d);
    d = calcAbsGregorianDays(d, m, y);
    int month, day, year;

    // Search forward year by year from approximate year
    year = (d - IslamicEpoch) / 355;

    while (d >= IslamicDate(1, 1, year))
        year++;

    year--;
    // Search forward month by month from Muharram
    month = 1;
    while (d > IslamicDate(month, LastDayOfIslamicMonth(month, year), year))
        month++;

    day = d - IslamicDate(month, 1, year) + 1;

    IslamicDateStruct ret(year,month,day);
    return ret;
    //cout << day << " " << getMonthName(month) << " " << year << " AH" << endl;
}
