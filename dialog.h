#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <vector>
#include<qmessagebox.h>
#include<qstandarditemmodel.h>
#include<qtimer.h>
#include<QDate>
namespace Ui {
class Dialog;
}

struct IslamicDateStruct
{
    int y_,m_,d_;
    IslamicDateStruct(int y,int m,int d):y_(y),m_(m),d_(d){}
    IslamicDateStruct(QDate d1)
    {
        d1.getDate(&y_,&m_,&d_);
    }

    operator QDate()
    {
        QDate q(y_,m_,d_);
        auto d=d_;
        while(0==q.toString().toStdString().length())
        {
            printf("This date cannot be showed using gregorian calendar control, we will sub day(s)\n");
            --d_;   //try a date behind
            q.setDate(y_,m_,d_);
        }
        d_=d;
        return q;
    }
};

IslamicDateStruct PrintCalender(const QDate &qd);

class Dialog : public QDialog
{
    Q_OBJECT
friend void updateTextBoxes(Dialog *diag,QModelIndex index);
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
private slots:
    void on_pushButton_released();

    void on_listView_2_clicked(const QModelIndex &index);

    void on_listView_clicked(const QModelIndex &index);

    void on_pushButton_2_released();

    void TimerUpdate();

    void on_pushButton_3_clicked();

    void on_pushButton_4_released();

    void on_pushButton_5_released();

private:
    Ui::Dialog *ui;
    std::vector<QDate> m_qdateList;
    QTimer m_timer;

    void save();
    void load();
};

#endif // DIALOG_H
