#-------------------------------------------------
#
# Project created by QtCreator 2016-03-26T23:54:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HijriCalendar
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h

FORMS    += dialog.ui

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -fpermissive
